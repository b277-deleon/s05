SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactFirstName, contactLastName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state is NULL;

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

SELECT employees.firstName, employees.lastName, offices.city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.city = "Tokyo";

SELECT customers.customerName FROM customers JOIN employees ON salesRepEmployeeNumber = employeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

SELECT products.productName, customers.customerName FROM customers JOIN orders ON customers.customerNumber = orders.customerNumber JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber JOIN products ON orderdetails.productCode = products.productCode where customers.customerName = "Baane Mini Imports";

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber JOIN offices ON customers.country = offices.country WHERE customers.country = offices.country;

SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

SELECT customerName FROM customers WHERE phone LIKE "%+81%";